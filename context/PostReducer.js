import {
	FETCH_POSTS,
	FETCH_POST
} from './PostTypes'

export default (state, {type, payload}) => {
	switch (type) {
		case FETCH_POSTS:
			return {
				...state,
				isAuthenticated: false,
				data: payload,
				item: ''
			}
		case FETCH_POST:
			return {
				...state,
				isAuthenticated: false,
				item: payload
			}
		default:
			break;
	}
}