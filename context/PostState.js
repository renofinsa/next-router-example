import React, { createContext, useReducer, useState, useEffect } from 'react'
export const PostContext = createContext();
import PostReducer from './PostReducer'

import {
	FETCH_POSTS,
	FETCH_POST
} from './PostTypes'

const PostState = ({ children }) => {
	const initialState = {
		isAuthenticated: false,
		data: [],
		item: [],
	}

	const [loading, setLoading] = useState(1)
	const [state, dispatch] = useReducer(PostReducer, initialState)

	const get_posts = async () => {
		await setLoading(1)

		try {
			fetch('https://jsonplaceholder.typicode.com/posts')
			.then(response => response.json())
			.then(json => {
			 dispatch({ type: FETCH_POSTS, payload: json})
			 setLoading(0)
			})

		} catch (error) {
			console.log(error.message);			
		}
	}

	const get_post = async (id) => {
		await setLoading(1)

		try {
			fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
			.then(response => response.json())
			.then(json => {
			 dispatch({ type: FETCH_POST, payload: json})
			 setLoading(0)
			})

		} catch (error) {
			console.log(error.message);			
		}
	}

	const {
	isAuthenticated,
	data,
	item,
	} = state

	return (
		<PostContext.Provider
			value={{
				data,
				loading,
				state,
				item,
				get_posts,
				get_post
			}}
		>
			{children}
		</PostContext.Provider>
	)
}

export default PostState
