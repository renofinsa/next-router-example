import '../styles/globals.css'
import PostState from './../context/PostState'

function MyApp({ Component, pageProps }) {
  return (
		<PostState>
			<Component {...pageProps} />
		</PostState>
	)
	
}

export default MyApp
