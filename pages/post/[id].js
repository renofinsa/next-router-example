import { useRouter } from 'next/router'
import { useContext, useEffect, useState } from 'react'
import { PostContext } from './../../context/PostState'

function Post({ }) {
	const router = useRouter()
  const { id } = router.query
	const [loading, setLoading] = useState(1)

	const {
		data,
		state,
		get_post
	} = useContext(PostContext)

	useEffect(() => {
			if (id) {
				get_post(id)
				setLoading(0)
			}
		return () => {
			
		}
	}, [id])

	if (loading) {
		return <h1>Loading ...</h1>
	}

	return (
		<div>
			<h1>{state.item.title}</h1>
			<p>{state.item.body}</p>
		</div>
	)
}


// Post.getInitialProps = async ({ query }) => {
	// const router = useRouter()
  // return { id: router.query }
// }


export default Post
