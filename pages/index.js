import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import { useContext, useEffect, useState } from 'react'
import { PostContext} from './../context/PostState'


function Home({ posts }) {

	const [loading, setLoading] = useState(1)

	const {
		data,
		state,
		get_posts
	} = useContext(PostContext)

	useEffect(() => {
		get_posts()
		setLoading(0)
	}, [])

	if (loading) {
		return <h1>Loading ...</h1>
	}

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

			{data.map((item, index) => (
				<Link key={index} href="/post/[id]" as={`post/${item.id}`}>
					<a>{item.title}
						<hr />
					</a>
				</Link>
			))}

    </div>
  )
}

// Home.getInitialProps = async () => {

// }

export default Home